//
//  ViewController.swift
//  FacebookLoginDemo_Inicio
//
//  Created by Diego Angel Fernandez Garcia on 09/04/2019.
//  Copyright © 2019 EfectoApple. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class ViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let firebaseAuth = Auth.auth()
        let vista = self.restorationIdentifier
        if (firebaseAuth.currentUser != nil && vista == "InicioViewController"){
            if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") {
                UIApplication.shared.keyWindow?.rootViewController = viewController
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginWithFacebook(_ sender: Any) {
        //1.
        let fbLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            
            //2.
            if let error = error {
                print("Failed to login: \(error.localizedDescription)")
                return
            }
            
            //2.
            guard let accessToken = FBSDKAccessToken.current() else {
                print("Failed to get access token")
                return
            }
            
            //3.
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
            
            //4.
            Auth.auth().signInAndRetrieveData(with: credential, completion: { (user, error) in
                //5.
                if let error = error {
                    print("Login error: \(error.localizedDescription)")
                    let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
                    let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(okayAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    return
                }
                
                //6.
                if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") {
                    UIApplication.shared.keyWindow?.rootViewController = viewController
                    self.dismiss(animated: true, completion: nil)
                }
                
            })
            
        }
    }
    @IBAction func logOut(_ sender: Any) {
         let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            FBSDKLoginManager().logOut()
            
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
            
        }
        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "InicioViewController") {
            UIApplication.shared.keyWindow?.rootViewController = viewController
        dismiss(animated: true, completion: nil)
        }
    }
    
}


